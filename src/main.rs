use std::error;

mod Crypto {


    #[derive(Copy, Clone)]
    pub struct Bicoder {

    }

    impl Bicoder {

        pub fn new() -> Bicoder{
            Bicoder{
            }
        }

        pub fn valid_hex_string(self, to_validate: &str) -> bool{

            if to_validate.len() % 2 != 0 {
                return false
            }

            let valid_hex_chars: [char;16] = ['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f'];
            let to_validate = String::from(to_validate);
            to_validate.chars().all(|x|{
                valid_hex_chars.contains(&x)
            })
        }
        pub fn to_decimal(&self, c: &char) -> Result<u8, Error::InvalidHexCharError> {
            match c {
                '0' => Ok(0),
                '1' => Ok(1),
                '2' => Ok(2),
                '3' => Ok(3),
                '4' => Ok(4),
                '5' => Ok(5),
                '6' => Ok(6),
                '7' => Ok(7),
                '8' => Ok(8),
                '9' => Ok(9),
                'a' => Ok(10),
                'b' => Ok(11),
                'c' => Ok(12),
                'd' => Ok(13),
                'e' => Ok(14),
                'f' => Ok(15),
                _ => Err(Error::InvalidHexCharError{})

            }
        }
        pub fn hex_string_to_ascii(&self, s: &str) -> String {
            let mut ascii_string:Vec<char> = vec![];
            let mut buffer : [char;2] = ['0','0'];
            for (i,ch) in s.chars().enumerate(){
                buffer[i % 2] = ch;
                if i % 2 == 1 {
                    let first_value = self.to_decimal(buffer.get(0).unwrap()).unwrap() * 16;
                    let second_value = self.to_decimal(buffer.get(1).unwrap()).unwrap();
                    let value = first_value + second_value;
                    ascii_string.push(value as char);
                }
            }
            ascii_string.into_iter().collect()
        }

        pub fn to_binary_string(&self,mut i: u8) -> &str{
            let bin_str: Vec<char> = vec![];
            while i > 1{
                let quotient = i / 2;

                i = i / 2;
            }
            String::from_iter(bin_str.iter()).as_str();
        }

        pub fn to_base64(&self, data: &[u8]) -> &[u8]{

        }

    }

    mod Error {
        #[derive(Debug, Clone)]
        pub struct InvalidHexCharError {}
        impl std::error::Error for InvalidHexCharError {
            fn description(&self) -> &str {
                "cannot represent given char as decimal value"
            }
            fn cause(&self) -> Option<& std::error::Error> {
                None
            }
        }
        impl std::fmt::Display for InvalidHexCharError {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result{
                write!(f, "cannot represent given char as decimal value")
            }
        }
    }
}

fn main() {
    let mut args = std::env::args();

    if args.len() != 2 {
        println!("Unexpected arguments length");
        std::process::exit(0);
    }

    let to_parse = args.nth(1).unwrap();
    let bic = Crypto::Bicoder::new();

    if !bic.valid_hex_string(to_parse.as_str()) {
        println!("Invalid hex chars");
        std::process::exit(0);
    }

    let ascii_string = bic.hex_string_to_ascii(&to_parse);
    //ascii_string.as_bytes());

}

mod tests {
    use super::*;

    #[test]
    fn test_valid_hex_string(){
        let mut bic = Crypto::Bicoder::new();
        assert_eq!(bic.valid_hex_string("1234567890abcdef"),true);
        assert_eq!(bic.valid_hex_string("11"),true);
        assert_eq!(bic.valid_hex_string("111"),false);
    }

    #[test]
    fn test_valid_hex_string_invalid(){
        let bic = Crypto::Bicoder::new();
        assert_eq!(bic.valid_hex_string("1234567890abcdefg"), false);
    }

}

